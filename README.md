# SolaxCloud-Symcon

Holt Daten aus der SolaxCloud mit Hilfe der Solax API. Benötigt einen Wechselrichter mit Kommunikationsmodul, welches die Daten schön brav in die chinesische SolaxCloud schiebt.

Als Parameter werden ein Token sowie die UID des Kommunikationsmodules des Solax-Wechselrichters benötigt. Beide sind im SolaxCloud-Portal zu finden (tokenId: linkes Menü "Bedienung"=>"API", sn: "Inv Management" -> "Registrationsnummer").