<?php

/**
 * Symcon Module to fetch data from SolaxCloud, API Version 1.0
 *
 * Version 0.2b BETA, 2020-08-04
 *
 * (C) 2020 Marek Kretzschmar (marekre)
 */

class SolaxCloud extends IPSModule {
    const debug = false;

    const INVERTER_TYPES = array(
                    1=>"X1-LX",
                    2=>"X-Hybrid",
                    3=>"X1-Hybiyd/Fit",
                    4=>"X1-Boost/Air/Mini",
                    5=>"X3-Hybiyd/Fit",
                    6=>"X3-20K/30K",
                    7=>"X3-MIC/PRO",
                    8=>"X1-Smart",
                    9=>"X1-AC",
                    10=>"A1-Hybrid",
                    11=>"A1-Fit",
                    12=>"A1-Grid",
                    13=>"J1-ESS");

    const INVERTER_STATUS = array(
                    100=>"Wait Mode",
                    101=>"Check Mode",
                    102=>"Normal Mode",
                    103=>"Fault Mode",
                    104=>"Permanent Fault Mode",
                    105=>"Update Mode",
                    106=>"EPS Check Mode",
                    107=>"EPS Mode",
                    108=>"Self-Test Mode",
                    109=>"Idle Mode",
                    110=>"Standby Mode",
                    111=>"Pv Wake Up Bat Mode",
                    112=>"Gen Check Mode",
                    113=>"Gen Run Mode");

    const REQUEST_ENABLED = array("Always"=>1, "OnlyAtDaylight"=>2, "Never"=>3);

    /**
     * 
     */
    public function __construct($InstanceID) {
        parent::__construct($InstanceID);
    }

    /**
     *
     */
    public function Create() {
        parent::Create();

        $refreshTimerInterval = 60; // Sekunden
        $daylightOffset = 60; // Minuten

        // Properties
        $this->RegisterPropertyString("api", "1.0");
        $this->RegisterPropertyString("tokenId", "");
        $this->RegisterPropertyString("sn", "");
        $this->RegisterPropertyInteger("refreshTimer", $refreshTimerInterval); //Sekunden
        $this->RegisterPropertyInteger("requestEnabled", self::REQUEST_ENABLED["Always"]);
        $this->RegisterPropertyInteger("daylightOffset", $daylightOffset); //Minuten

        // Variables
        $this->RegisterVariableString("exception", "exception", "", 100);
        $this->RegisterVariableString("success", "success", "", 101);
        $this->RegisterVariableString("inverterSN", "Seriennummer", "", 1);
        $this->RegisterVariableString("sn", "UID Kommunikationsmodul", "", 1);
        $this->RegisterVariableInteger("acpower", "AC Power", "", 2);
        $this->RegisterVariableFloat("yieldtoday", "inverter.AC.energy.out.daily", "", 3);
        $this->RegisterVariableFloat("yieldtotal", "inverter.AC.energy.out.total", "", 4);
        $this->RegisterVariableInteger("feedinpower", ".power.total", "", 5);
        $this->RegisterVariableFloat("feedinenergy", "energy.toGrid.total", "", 6);
        $this->RegisterVariableFloat("consumeenergy", ".energy.fromGrid.total", "", 7);
        $this->RegisterVariableInteger("feedinpowerM2", "address2meter.AC.power.total", "", 8);
        $this->RegisterVariableInteger("soc", "inverter.DC.battery.energy.SOC", "", 9);
        $this->RegisterVariableFloat("peps1", "inverter.AC.EPS.power.R", "", 10);
        $this->RegisterVariableFloat("peps2", "inverter.AC.EPS.power.S", "", 11);
        $this->RegisterVariableFloat("peps3", "inverter.AC.EPS.power.T", "", 12);
        $this->RegisterVariableFloat("inverterType", "inverterType", "", 13);
        $this->RegisterVariableInteger("inverterStatus", "inverterStatus", "", 14);
        $this->RegisterVariableString("inverterTypeText", "Typ", "", 13);
        $this->RegisterVariableString("inverterStatusText", "Status", "", 14);
        $this->RegisterVariableString("uploadTime", "Update time", "", 15);

        $this->RegisterTimer("refreshTimer", $refreshTimerInterval * 1000, 'SLX_getStatus($_IPS[\'TARGET\']);'); // Intervall in ms --> * 1000 ==> Sekunden

    }

    // Überschreibt die intere IPS_ApplyChanges($id) Funktion
    public function ApplyChanges() {
        parent::ApplyChanges();

        $api = $this->ReadPropertyString("api");


        $tokenId = $this->ReadPropertyString("tokenId");
        $sn = $this->ReadPropertyString("sn");
        
        // Validate tokenId and sn
//TODO

        
        $refreshTimerInterval = $this->ReadPropertyInteger("refreshTimer");
        $this->SetTimerInterval("refreshTimer", $refreshTimerInterval * 1000);

        $requestEnabled = $this->ReadPropertyInteger("requestEnabled");
        $daylightOffset = $this->ReadPropertyInteger("daylightOffset");
    
    }


    /**
     *
     */
    public function getStatus()
    {

        // verify daylight settings
        $requestEnabled = $this->ReadPropertyInteger("requestEnabled");


        switch($requestEnabled) {
            case 1: // Always
                $this->fetchData();
            break;
            case 2: // Fetch data only if "isDay"
                $LocationID = IPS_GetInstanceListByModuleID("{45E97A63-F870-408A-B259-2933F7EABF74}")[0];
                $IstEsTagID = IPS_GetObjectIDByIdent("IsDay", $LocationID);        
                $isDay = GetValueBoolean($IstEsTagID);
                if($isDay) {
                    $this->fetchData();
                }
            break;
            case 3: // Off
            break;
        }
    }


    public function fetchData() {
        $tokenId = $this->ReadPropertyString("tokenId");
        $sn = $this->ReadPropertyString("sn");
        
        $url="https://www.eu.solaxcloud.com:9443/proxy/api/getRealtimeInfo.do?tokenId={$tokenId}&sn={$sn}";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        // Execute
        $result=curl_exec($ch);
        curl_close($ch);

        $data = json_decode($result, true);

        $main_fields = array("exception", "success");
        foreach ($main_fields as &$field) {
            $$field=$data[$field];
            $this->SetValue($field, $$field);
        }

        $result_fields = array("inverterSN",
                               "sn",
                               "acpower",
                               "yieldtoday",
                               "yieldtotal",
                               "feedinpower",
                               "feedinenergy",
                               "consumeenergy",
                               "feedinpowerM2",
                               "soc",
                               "peps1",
                               "peps2",
                               "peps3",
                               "inverterType",
                               "inverterStatus",
                               "uploadTime");
        foreach ($result_fields as &$field) {
            $$field=$data["result"][$field];
            $this->SetValue($field, $$field);
        }

        $inverterType = $data["result"]["inverterType"];
        $inverterStatus =  $data["result"]["inverterStatus"];

        $this->SetValue("inverterTypeText", self::INVERTER_TYPES[$inverterType]);
        $this->SetValue("inverterStatusText", self::INVERTER_STATUS[$inverterStatus]);
    }
}

?>
